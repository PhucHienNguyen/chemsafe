import Blog from '../models/index.js';
import fs from 'fs';




export function getFile(req, res, next){
  fs.readdir('public'+req.body.directory, (err, files) => {
    console.log(files);
    return res.status(200).json({
      success: true,
      message: '',
      data: files.filter(x => x != '.DS_Store').map(x =>
        req.body.directory+ "/" + x)
    });
  });
}
/**
 * get blogs
 */
export function getAllBlog(req, res, next) {
  Blog.find().sort({
    name: 1,
  }).exec((err, blogs) => {
    if (err) return next(err);
    console.log('getAllBlog(blogs): ', blogs);
    return res.status(200).json({
      success: true,
      message: 'Get all blog',
      data: blogs,
    });
  });
}

/**
 * add blog
 */
export function addBlog(req, res, next) {
  console.log('addBlog(req.body): ', req.body);
  let newBlog = new Blog();

  Object.assign(newBlog, req.body);
  console.log(req.body);
  console.log('addBlog(newBlog): ', newBlog);
  newBlog.save(function (err, blog) {
    if (err) return next(err);

    return res.status(201).json({
      success: true,
      message: 'Created blog!',
      data: blog,
    });
  });
}

/**
 * get blog
 */
export function getBlog(req, res, next) {
  console.log("ABC",req.body.name);
  Blog.find({$or:[{'name': {'$regex': req.body.name, $options: 'i'}}]}).sort({
    name: 1,
  }).exec((err, blogs) => {
    if (err) return next(err);
    console.log('getSomeBlog(blogs): ', blogs);
    return res.status(200).json({
      success: true,
      message: 'Get some blog',
      data: blogs,
    });
  });
}

export function getOneBlog(req, res, next){
  console.log(req.body.params);
  Blog.findOne({'_id': req.body.params.id}).exec((err,blog) => {
    if (err) return next(err);
    console.log('getOneBlog(blog): ', blog);
    return res.status(200).json({
      success: true,
      message: 'Get one blog',
      data: blog,
    });
  });
}
