import mongoose, { Schema } from 'mongoose';

// create a blogSchema
export const blogSchema = new Schema({
  name: {
    type: String,
    required: [true, 'name is required.'],
  },
  cthh: {
    type: String,
  },
  dbocchay: {
    type: String,
  },
  dsang: {
    type: String,
  },
  gioihanno: {
    type: String,
  },
  ld50: {
    type: String,
  },
  lc50: {
    type: String,
  },
  pel: {
    type: String,
  },
  rel: {
    type: String,
  },
  idlh: {
    type: String,
  },
  thghs: {
    type: String,
  },
  ncghs: {
    type: String,
  },
  trieuchung: {
    type: String,
  },
  bcphongnguask: {
    type: String,
  },
  txmat: {
    type: String,
  },
  txda: {
    type: String,
  },
  hit: {
    type: String,
  },
  nuot: {
    type: String,
  },
  bcphongngua: {
    type: String,
  },
  chay: {
    type: String,
  },
  trannho: {
    type: String,
  },
  tranlon: {
    type: String,
  },
  luutru: {
    type: String,
  },
}, { versionKey: false });

/*
 * userSchema middlewares
 */

// on every save, add the date
blogSchema.pre('save', function (next) {
  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updatedAt = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.createdAt) {
    this.createdAt = currentDate;
  }

  next();
});

// the schema is useless so far
// we need to create a model using it
const Blog = mongoose.model('Blog', blogSchema);

// make this available to our users in our Node applications
export default Blog;
