import express from 'express';

import { getAllBlog, addBlog, getBlog, getOneBlog, getFile } from '../controllers/index.js';

const blogRoutes = express.Router();

blogRoutes.route('/')
  .get(getAllBlog);


blogRoutes.route('/blog')
  .post(getOneBlog);

blogRoutes.route('/getFile')
  .post(getFile);

blogRoutes.route('/addblog')
  .post(addBlog);

blogRoutes.route('/search')
  .post(getBlog);



export default blogRoutes;
