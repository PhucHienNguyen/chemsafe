import express from 'express';

import { getAllUser, addUser, getUser } from '../controllers/index.js';

const userRoutes = express.Router();

userRoutes.route('/')
  .get(getAllUser)
  .post(addUser);

userRoutes.route('/:_id')
  .get(getUser);

export default userRoutes;
