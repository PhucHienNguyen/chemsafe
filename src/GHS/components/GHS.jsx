import React, { Component } from 'react';
import toastr from 'toastr';

class GHSComponent extends Component {
  render() {
    return (
      <div className='row'>
        <h1 className='text-center'>GHS</h1>
        <div className='col-xs-10 col-xs-offset-1 text-justify'>

          <p>Cấu trúc của GHS dựa trên sự ủy quyền của “Hội nghị các quốc gia về môi trường và phát triển”
           ( United Nations Conference on Environment and Development (UNCED)) năm 1992, với sự phát triển
           của hệ thống này được đề cập ở đoạn 26 và 27 của chương trình nghị sự 29, phần 19, vấn đề B, được
           trích dẫn như sau:
          </p>
          <p>26. Hệ thống phân loại và dán nhãn mối nguy hiểm toàn cầu chưa có sẵn để đẩy mạnh việc sử dụng
          an toàn hóa chất, không kể những cái khác, ở nơi làm việc hay ở nhà. Sự phân loại hóa chất có thể
          được thực hiện cho nhiều mục đích khác nhau và nói riêng là một cụ quan trọng để thiết lập hệ thống
          nhãn. Có một sự cần thiết để phát triển hệ thống phân loại và dán nhãn mối nguy hiểm toàn cầu, xây
          dựng những công việc đang xảy ra.
          </p>
          <p>27. Hệ thống phân loại và dán nhãn mối nguy hiểm toàn cầu bao gồm dữ liệu an toàn vật liệu và
           một cách dễ dàng các kí hiệu hiểu biết, nên có sẵn cho đến năm 2000.
          </p>
        </div>
      </div>
    );
  }
}

export default GHSComponent;
