import React from 'react';
import { Route, IndexRedirect } from 'react-router';
import toastr from 'toastr';

import GHSContainer from '../containers/GHS.jsx';


export default function () {
  return (
    <Route path='GHS'>
      <IndexRedirect to='index' />
      <Route path='index' component={GHSContainer} />
    </Route>
  );
}
