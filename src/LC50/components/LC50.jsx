import React, { Component } from 'react';
import toastr from 'toastr';

class LC50Component extends Component {
  render() {
    return (
      <div className='row'>
        <h1 className='text-center'>LC<sub>50 </sub></h1>
        <div className='col-xs-10 col-xs-offset-1 text-justify'>

          <p>Đối với đường hô hấp người ta thường tính nồng độ gây chết (lethal concertration).
          LC<sub>50 </sub> là nồng độ của một chất trong không khí, trong đất hoặc nước có khả năng giết chết 50% súc
           vật thử nghiệm trong một khoảng thời gian nhất định.
          </p>
        </div>
      </div>
    );
  }
}

export default LC50Component;
