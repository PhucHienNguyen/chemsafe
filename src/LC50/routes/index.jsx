import React from 'react';
import { Route, IndexRedirect } from 'react-router';
import toastr from 'toastr';

import LC50Container from '../containers/LC50.jsx';


export default function () {
  return (
    <Route path='LC50'>
      <IndexRedirect to='index' />
      <Route path='index' component={LC50Container} />
    </Route>
  );
}
