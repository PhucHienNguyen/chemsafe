import React, { Component } from 'react';
import toastr from 'toastr';

class LD50Component extends Component {
  render() {
    return (
      <div className='row'>
        <h1 className='text-center'>Liều độc cấp tính LD<sub>50 </sub></h1>
        <div className='col-xs-10 col-xs-offset-1 text-justify'>

          <p>Liều độc cấp tính LD<sub>50</sub> (mg/kg) là liều lượng có thể giết chết 50/100 súc vật thử nghiệm.
           LD<sub>50</sub> có thể xác định bằng nhiều đườn dùng thuốc, thông thường bằng đường uống hay qua da.
           Các chất độc được thử nghiệm với các liều khác nhau trên cùng một loài thú vật.
           Các loài thú vật này đươch chia thành từng nhóm và mỗi nhóm sẽ cho dùng một liều.
          </p>
        </div>
      </div>
    );
  }
}

export default LD50Component;
