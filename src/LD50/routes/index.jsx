import React from 'react';
import { Route, IndexRedirect } from 'react-router';
import toastr from 'toastr';

import LD50Container from '../containers/LD50.jsx';


export default function () {
  return(
    <Route path='LD50'>
      <IndexRedirect to='index' />
      <Route path='index' component={LD50Container} />
    </Route>
  );
}
