import React, { Component } from 'react';
import toastr from 'toastr';

class NCGHSComponent extends Component {
  render() {
    return (
      <div className='row'>
        <div className='col-xs-10 col-xs-offset-1 text-center'>
        <div className='table-responsive'>
          <table className='table table-bordered table-hover'>
            <thead>
              <tr >
                <th className='text-center' colSpan={4}><h4><b>Kí hiệu trạng thái nguy hiểm với sự nguy hiểm sức khỏe.
                </b></h4></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className='min'><b>Ký hiệu</b></td>
                <td className='set'><b>Trạng thái nguy hiểm sức khỏe</b></td>
                <td className='set'><b>Loại nguy hiểm</b></td>
                <td className='min'><b>Nhóm độc</b></td>
              </tr>
              <tr>
                <td className='min'>H300</td>
                <td className='text-justify'>Gây nguy hiểm khi nuốt phải.</td>
                <td className='text-justify'>Độc tính cấp tính, qua đường miệng</td>
                <td className='min'>1,2</td>
              </tr>
              <tr>
                <td className='min'>H301</td>
                <td className='text-justify'>Độc khi nuốt phải.</td>
                <td className='text-justify'>Độc tính cấp tính, qua đường miệng</td>
                <td className='min'>3</td>
              </tr>
              <tr>
                <td className='min'>H302</td>
                <td className='text-justify'>Gây hại khi nuốt phải.</td>
                <td className='text-justify'>Độc tính cấp tính, qua đường miệng</td>
                <td className='min'>4</td>
              </tr>
              <tr>
                <td className='min'>H303</td>
                <td className='text-justify'>Có lẽ gây hại khi nuốt phải.</td>
                <td className='text-justify'>Độc tính cấp tính, qua đường miệng</td>
                <td className='min'>5</td>
              </tr>
              <tr>
                <td className='min'>H304</td>
                <td className='text-justify'>Có lẽ gây nguy hiểm khi nuốt phải và thâm nhập qua đường khí.</td>
                <td className='text-justify'>Nguy hiểm về hô hấp</td>
                <td className='min'>1</td>
              </tr>
              <tr>
                <td className='min'>H304</td>
                <td className='text-justify'>Có lẽ gây nguy hiểm khi nuốt phải và thâm nhập qua đường khí.</td>
                <td className='text-justify'>Nguy hiểm về hô hấp</td>
                <td className='min'>1</td>
              </tr>
              <tr>
                <td className='min'>H305</td>
                <td className='text-justify '>Có lẽ gây hại khi nuốt phải và thâm nhập qua đường khí.</td>
                <td className='text-justify'>Nguy hiểm về hô hấp</td>
                <td className='min'>2</td>
              </tr>
              <tr>
                <td className='set' colSpan={4}></td>
              </tr>
              <tr>
                <td className='min'>H310</td>
                <td className='text-justify '>Nguy hiểm liên qua đến da.</td>
                <td className='text-justify'>Độc tính cấp tính, qua da</td>
                <td className='min'>1,2</td>
              </tr>
              <tr>
                <td className='min'>H311</td>
                <td className='text-justify '>Độc liên qua đến da.</td>
                <td className='text-justify'>Độc tính cấp tính, qua da</td>
                <td className='min'>3</td>
              </tr>
              <tr>
                <td className='min'>H312</td>
                <td className='text-justify '>Gây hại liên qua đến da.</td>
                <td className='text-justify'>Độc tính cấp tính, qua da</td>
                <td className='min'>4</td>
              </tr>
              <tr>
                <td className='min'>H313</td>
                <td className='text-justify '>Có lẽ gây hại liên qua đến da.</td>
                <td className='text-justify'>Độc tính cấp tính, qua da</td>
                <td className='min'>5</td>
              </tr>
              <tr>
                <td className='min'>H314</td>
                <td className='text-justify '>Gây bỏng da mạnh và huy hiểm đến mắt.</td>
                <td className='text-justify'>Ăn mòn da và làm rát da.</td>
                <td className='min'>1A,1B,1C</td>
              </tr>
              <tr>
                <td className='min'>H315</td>
                <td className='text-justify'>Gây rát da.</td>
                <td className='text-justify'>Ăn mòn da và làm rát da.</td>
                <td className='min'>2</td>
              </tr>
              <tr>
                <td className='min'>H316</td>
                <td className='text-justify '>Gây rát da nhẹ.</td>
                <td className='text-justify'>Ăn mòn da và làm rát da.</td>
                <td className='min'>3</td>
              </tr>
              <tr>
                <td className='min'>H317</td>
                <td className='text-justify '>Có lẽ gây ra phản ứng dị ứng với da.</td>
                <td className='text-justify'>Gây nhạy cảm cho da.</td>
                <td className='min'>1,1A,1B</td>
              </tr>
              <tr>
                <td className='min'>H318</td>
                <td className='text-justify '>Gây ra mối đe dọa nghiêm trọng với mắt.</td>
                <td className='text-justify'>Đe dọa nghiêm trọng tới mắt, rát mắt</td>
                <td className='min'>1</td>
              </tr>
              <tr>
                <td className='min'>H319</td>
                <td className='text-justify'>Gây ra sự rát mắt nghiêm trọng.</td>
                <td className='text-justify'>Đe dọa nghiêm trọng tới mắt, rát mắt</td>
                <td className='min'>2A</td>
              </tr>
              <tr>
                <td className='set' colSpan={4}></td>
              </tr>
              <tr>
                <td className='min'>H320</td>
                <td className='text-justify'>Gây ra sự rát mắt.</td>
                <td className='text-justify'>Đe dọa nghiêm trọng tới mắt, rát mắt</td>
                <td className='min'>2B</td>
              </tr>
              <tr>
                <td className='set' colSpan={4}></td>
              </tr>
              <tr>
                <td className='min'>H330</td>
                <td className='text-justify'>Gây nguy hiểm khi hít phải</td>
                <td className='text-justify'>Độc tính cấp tính, qua đường hô hấp.</td>
                <td className='min'>1,2</td>
              </tr>
              <tr>
                <td className='min'>H331</td>
                <td className='text-justify'>Độc khi hít phải</td>
                <td className='text-justify'>Độc tính cấp tính, qua đường hô hấp.</td>
                <td className='min'>3</td>
              </tr>
              <tr>
                <td className='min'>H332</td>
                <td className='text-justify'>Gây hại khi hít phải</td>
                <td className='text-justify'>Độc tính cấp tính, qua đường hô hấp.</td>
                <td className='min'>4</td>
              </tr>
              <tr>
                <td className='min'>H333</td>
                <td className='text-justify'>Có lẽ gây hại khi hít phải.</td>
                <td className='text-justify'>Độc tính cấp tính, qua đường hô hấp.</td>
                <td className='min'>5</td>
              </tr>
              <tr>
                <td className='min'>H334</td>
                <td className='text-justify'>Gây ra dị ứng hoặc triệu chứng hen suyễn hoặc khó khăn trong việc thở nếu hít phải</td>
                <td className='text-justify'>Gây nhạy cảm về hô hấp.</td>
                <td className='min'>1,1A,1B</td>
              </tr>
              <tr>
                <td className='min'>H335</td>
                <td className='text-justify'>Gây ra sự rát nhạy cảm</td>
                <td className='text-justify'>Gây độc các cơ quan đặc hiệu, phơi nhiễm riêng lẻ, gây nhạy cảm ống hô hấp.</td>
                <td className='min'>3</td>
              </tr>
              <tr>
                <td className='min'>H336</td>
                <td className='text-justify'>Có lẽ gây uể oải, choáng váng</td>
                <td className='text-justify'>Gây độc các cơ quan đặc hiệu, phơi nhiễm riêng lẻ, gây nhạy cảm ống hô hấp.</td>
                <td className='min'>3</td>
              </tr>
              <tr>
                <td className='set' colSpan={4}></td>
              </tr>
              <tr>
                <td className='min'>H340</td>
                <td className='text-justify'>Có lẽ gây ra những khuyết điểm di truyền.</td>
                <td className='text-justify colorRed'>Tế bào nguyên sinh</td>
                <td className='min'>1A,1B</td>
              </tr>
              <tr>
                <td className='min'>H341</td>
                <td className='text-justify'>Nghi ngờ những khuyết điểm di truyền.</td>
                <td className='text-justify colorRed'>Tế bào nguyên sinh</td>
                <td className='min'>2</td>
              </tr>
              <tr>
                <td className='set' colSpan={4}></td>
              </tr>
              <tr>
                <td className='min'>H350</td>
                <td className='text-justify'>Có lẽ gây ra ung thư</td>
                <td className='text-justify'>Chất sinh ung thư</td>
                <td className='min'>1A,1B</td>
              </tr>
            </tbody>
          </table>
        </div>
        </div>

      </div>
    );
  }
}

export default NCGHSComponent;
