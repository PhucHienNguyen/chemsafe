import React from 'react';
import { Route, IndexRedirect } from 'react-router';
import toastr from 'toastr';

import NCGHSContainer from '../containers/NCGHS.jsx';


export default function () {
  return (
    <Route path='NCGHS'>
      <IndexRedirect to='index' />
      <Route path='index' component={NCGHSContainer} />
    </Route>
  );
}
