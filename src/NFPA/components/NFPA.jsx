import React, { Component } from 'react';
import toastr from 'toastr';

class NFPAComponent extends Component {
  render() {
    return (
      <div className='row'>
        <h1 className='text-center'>NFPA 704</h1>
        <div className='col-xs-10 col-xs-offset-1 text-justify'>

          <p><strong>NFPA 704</strong> là một tiêu chuẩn được Hiệp hội phòng cháy quốc gia Hoa Kỳ đưa ra.
          Nó được nói đến một cách thông thường như là <strong>"hình thoi cháy"</strong>, được các nhân viên của bộ phận
          tình trạng khẩn cấp sử dụng để nhanh chóng và dễ dàng xác định các rủi ro gây ra bởi các hóa
          chất nguy hiểm ở gần đó. Nó là cần thiết để xác định các phương tiện và thiết bị (nếu có) nào
          là cần thiết và/hoặc các rủi ro/các thủ tục cần phải thực hiện
          </p>
          <p>Biểu tượng của NFPA được chia thành 4 phần thông thường là có màu mang ý nghĩa tượng trưng,
          trong đó màu xanh lam dùng để chỉ các nguy hiểm đối với sức khỏe, màu đỏ dùng để chỉ khả năng cháy,
          màu vàng dùng để chỉ khả năng phản ứng hóa học, và màu trắng chứa các mã đặc biệt cho các nguy hiểm
          lạ thường. Tất cả các khả năng gây nguy hiểm cho sức khỏe, khả năng cháy và phản ứng hóa học được
          đánh giá theo thang độ từ 0 (không nguy hiểm; chất thông thường) tới 4 (cực kỳ nguy hiểm).
          </p>
          <p><strong>Xanh lam/Sức khỏe</strong></p>
          <ul>
            <li>4. Phơi nhiễm trong thời gian cực ngắn có thể dẫn đến tử vong hay để lại các tổn thương
            lớn vĩnh cửu. (ví dụ xyanua hiđrô)</li>
            <li>3. Phơi nhiễm trong thời gian ngắn có thể dẫn tới các tổn thương nghiêm trọng tạm thời
            hay vĩnh cửu. (ví dụ khí clo)</li>
            <li>2. Phơi nhiễm mạnh hay liên tục nhưng không kinh niên có thể dẫn tới mất khả năng tạm
             thời hay các tổn thương nhất thời có thể nào đó. (ví dụ khí amôniắc)</li>
            <li>1. Sự phơi nhiễm có thể sinh ra kích thích nhưng chỉ có các tổn thương nhỏ.
            (ví dụ nhựa thông, butan)</li>
            <li>0. Sự phơi nhiễm trong điều kiện cháy không sinh ra các nguy hiểm mặc dù đó là các vật
             liệu cháy được. (ví dụ dầu lạc)</li>
          </ul>
          <p><strong>Đỏ/Khả năng cháy</strong></p>
          <ul>
            <li>4. Bay hơi nhanh chóng và toàn bộ ở nhiệt độ và áp suất thông thường, hoặc rất nhanh phân tán
            trong không khí và cháy rất nhanh. (ví dụ prôpan, butan)</li>
            <li>3. Các chất lỏng và chất rắn nào có thể bắt lửa dưới gần như mọi điều kiện nhiệt độ môi
            trường xung quanh. (ví dụ xăng)</li>
            <li>2. Phải có nhiệt độ môi trường xung quanh tương đối cao hay bị đốt nóng vừa phải trước khi có
            thể bắt lửa. (ví dụ dầu diesel)</li>
            <li>1. Phải đốt nóng trước khi có thể bắt lửa. (ví dụ tinh dầu cải dầu)</li>
            <li>0. Không cháy. (ví dụ nước)</li>
          </ul>
          <p><strong>Vàng/Khả năng phản ứng</strong></p>
          <ul>
            <li>4. Có khả năng nổ và phân hủy ở điều kiện nhiệt độ và áp suất bình thường. (ví dụ TNT)</li>
            <li>3. Có khả năng nổ và phân hủy nhưng đòi hỏi phải có nguồn kích thích mạnh, phải được đốt nóng trong
            không gian kín trước khi phản ứng, hoặc có phản ứng gây nổ với nước. (ví dụ flo)</li>
            <li>2. Tham gia phản ứng hóa học mãnh liệt ở nhiệt độ và áp suất cao, phản ứng mãnh liệt với nước,
            hay có thể tạo hỗn hợp nổ với nước. (ví dụ canxi)</li>
            <li>1. Thông thường là ổn định, nhưng có thể trở thành không ổn định ở nhiệt độ và áp suất cao.
            (ví dụ phốt pho đỏ)</li>
            <li>0. •	Thông thường là ổn định, thậm chí kể cả trong các điều kiện gần nguồn lửa, và không có
             phản ứng với nước. (ví dụ nitơ lỏng, butan)</li>
          </ul>
          <p><strong>Trắng/Đặc biệt</strong></p>
          <p>Khu vực "thông báo đặc biệt" màu trắng có thể chứa một vài biểu tượng sau:</p>
          <ul>
            <li>'<strike>W</strike>' - phản ứng với nước trong các tình huống bất thường hay nguy hiểm.</li>
            <li>'OX' - chất ôxi hóa</li>
          </ul>
          <p>Chỉ có '<strike>W</strike>' và 'OX' là phần chính thức của tiêu chuẩn NFPA 704, nhưng các biểu tượng tự giải
          thích khác thỉnh thoảng cũng được sử dụng trong các tình huống không chính thức.</p>
          <ul>
            <li>'COR' - chất ăn mòn; axít hay bazơ mạnh</li>
              <ul>
                <li>'ACID' và 'ALK' là cụ thể hơn.</li>
              </ul>
            <li>'BIO' - Các chất nguy hiểm sinh học</li>
            <li>Hình ba lá phóng xạ (<img src='/images/NFPA.png' width='20px' alt='' />) - là biểu tượng phóng xạ.</li>
          </ul>
        </div>
      </div>
    );
  }
}

export default NFPAComponent;
