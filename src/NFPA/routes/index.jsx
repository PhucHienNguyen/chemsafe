import React from 'react';
import { Route, IndexRedirect } from 'react-router';
import toastr from 'toastr';

import NFPAContainer from '../containers/NFPA.jsx';


export default function () {
  return (
    <Route path='NFPA'>
      <IndexRedirect to='index' />
      <Route path='index' component={NFPAContainer} />
    </Route>
  );
}
