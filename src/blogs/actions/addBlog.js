import axios from 'axios';

export const addBlog = ((object, callback) => {
  try {
    return function (dispatch) {
      dispatch({ type: 'ADD_BLOG_REQUEST' });
      axios.post('blogs/addblog', object)
        .then((response) => {
          dispatch({
            type: 'ADD_BLOG_SUCCESS',
            payload: response.data,
          });
          if (typeof callback === 'function') {
            callback(null, response.data);
          }
        })
        .catch((error) => {
          dispatch({ type: 'ADD_BLOG_FAILURE' });
          if (typeof callback === 'function') {
            callback(error.response.data, null);
          }
        });
    };
  } catch (e) {
    return res.status(500).json({
      success: false,
      message: 'Error in addBlog',
      error: e,
    });
  }
});
