import axios from 'axios';

export const getBlogbySearch = ((object, callback) => {
  try {
    return function (dispatch) {
      dispatch({ type: 'SEARCH_REQUEST' });
        axios.post('blogs/search',object)
        .then((response) => {
          dispatch({
            type: 'SEARCH_SUCCESS',
            payload: response.data,
          });
          if (typeof callback === 'function') {
            callback(null, response.data);
          }
        })
        .catch((error) => {
          dispatch({ type: 'SEARCH_FAILURE' });
          if (typeof callback === 'function') {
            callback(error.response.data, null);
          }
        });
    };
  } catch (e) {
    return res.status(500).json({
      success: false,
      message: 'Error in Search',
      error: e,
    });
  }
});


export const getInfoBlog = ((object, callback) => {
  try {
    return function (dispatch) {
      dispatch({ type: 'GET_INFO_REQUEST' });
        axios.post('blogs/blog',{params:{
          id: object
        }})
        .then((response) => {
          dispatch({
            type: 'GET_INFO_SUCCESS',
            payload: response.data,
          });
          if (typeof callback === 'function') {
            callback(null, response.data);
          }
        })
        .catch((error) => {
          dispatch({ type: 'GET_INFO_FAILURE' });
          if (typeof callback === 'function') {
            callback(error.response.data, null);
          }
        });
    };
  } catch (e) {
    return res.status(500).json({
      success: false,
      message: 'Error in getBlog',
      error: e,
    });
  }
});
export const getFile = ((object, callback) => {
  try {
    return function (dispatch) {
      dispatch({ type: 'GET_FILE_REQUEST' });
        axios.post('blogs/getFile',{directory : object
          })
        .then((response) => {
          dispatch({
            type: 'GET_FILE_SUCCESS',
            payload: response.data,
          });
          if (typeof callback === 'function') {
            callback(null, response.data);
          }
        })
        .catch((error) => {
          dispatch({ type: 'GET_FILE_FAILURE' });
          if (typeof callback === 'function') {
            callback(error.response.data, null);
          }
        });
    };
  } catch (e) {
    return res.status(500).json({
      success: false,
      message: 'Error in getBlog',
      error: e,
    });
  }
});
