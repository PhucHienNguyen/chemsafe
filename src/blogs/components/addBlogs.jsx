import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link, browserHistory } from 'react-router';
import toastr from 'toastr';

class AddBlogComponent extends Component {
  render() {
    return (
      <div className='row'>
        <div className='col-xs-12 text-center'>
          <h1 className='title'>Add New</h1>
        </div>
        <div className='col-xs-4 col-xs-offset-4'>
          <form onSubmit={this.handleSubmit.bind(this)}>
            <div className='form-group'>
              <label htmlFor='name'>Tên chất: </label>
              <input
                ref='name'
                type='text'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='cthh'>CTHH: </label>
              <input
                ref='cthh'
                type='text'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='imgGHS'>CTHH: </label>
              <input
                 name="imgGHS" 
                 type="file"
                 accept="image/x-png,image/gif,image/jpeg"
                 multiple="multiple" />
            </div>

            <div className='form-group'>
              <label htmlFor='dbocchay'>Điểm tự bốc cháy (°C):</label>
              <input
                ref='dbocchay'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='dsang'>Điểm sáng (°C):</label>
              <input
                ref='dsang'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='gioihanno'>Giới hạn nổ (%): </label>
              <input
                ref='gioihanno'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='ld50'>LD<sub>50 </sub> (mg/kg): </label>
              <textarea
                ref='ld50'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='lc50'>LC<sub>50 </sub> (ppm): </label>
              <input
                ref='lc50'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='pel'>PEL (ppm): </label>
              <input
                ref='pel'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='rels'>REL (ppm): </label>
              <textarea
                ref='rels'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='idlh'>IDLH (ppm): </label>
              <input
                ref='idlh'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='thghs'>Tín hiệu GHS: </label>
              <input
                ref='thghs'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='ncghs'>Nguy cơ GHS: </label>
              <input
                ref='ncghs'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='trieuchung'>Triệu chứng khi bị nhiễm: </label>
              <textarea
                ref='trieuchung'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='bcphongnguask'>Báo cáo phòng ngừa GHS (Về sức khoẻ): </label>
              <textarea
                ref='bcphongnguask'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='txmat'>Tiếp xúc với mắt: </label>
              <textarea
                ref='txmat'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='txda'>Tiếp xúc với da: </label>
              <textarea
                ref='txda'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='hit'>Hít phải: </label>
              <textarea
                ref='hit'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='nuot'>Nuốt phải: </label>
              <textarea
                ref='nuot'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='bcphongngua'>Báo cáo phòng ngừa GHS: </label>
              <textarea
                ref='bcphongngua'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='chay'>Sự cố cháy (theo GHS): </label>
              <textarea
                ref='chay'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='trannho'>Sự cố tràn nhỏ: </label>
              <textarea
                ref='trannho'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='tranlon'>Sự cố tràn lớn: </label>
              <textarea
                ref='tranlon'
                type='string'
                className='form-control' />
            </div>
            <div className='form-group'>
              <label htmlFor='luutru'>Lưu trữ  (theo GHS): </label>
              <textarea
                ref='luutru'
                type='string'
                className='form-control' />
            </div>
            <div className='col-xs-offset-6'>
              <button
                type='submit'
                className='btn btn-block btn-primary'>
                Submit
              </button>
            </div>
            <div className='text-center'>
              <p><br/>- OR -<br/></p>
              <Link
                to='/blogs/allblogs'
                className='btn btn-block btn-default'>

                Cancel
              </Link>
            </div>
          </form>
        </div>
      </div>
    );
  }

  handleSubmit(event) {
    const _this = this;
    event.preventDefault();

    let name = ReactDOM.findDOMNode(_this.refs.name).value.trim();
    let cthh = ReactDOM.findDOMNode(_this.refs.cthh).value.trim();
    let dbocchay = ReactDOM.findDOMNode(_this.refs.dbocchay).value.trim();
    let dsang = ReactDOM.findDOMNode(_this.refs.dsang).value.trim();
    let gioihanno = ReactDOM.findDOMNode(_this.refs.gioihanno).value.trim();
    let ld50 = ReactDOM.findDOMNode(_this.refs.ld50).value.trim();
    let lc50 = ReactDOM.findDOMNode(_this.refs.lc50).value.trim();
    let pel = ReactDOM.findDOMNode(_this.refs.pel).value.trim();
    let rel = ReactDOM.findDOMNode(_this.refs.rels).value.trim();
    let idlh = ReactDOM.findDOMNode(_this.refs.idlh).value.trim();
    let thghs = ReactDOM.findDOMNode(_this.refs.thghs).value.trim();
    let ncghs = ReactDOM.findDOMNode(_this.refs.ncghs).value.trim();
    let trieuchung = ReactDOM.findDOMNode(_this.refs.trieuchung).value.trim();
    let bcphongnguask = ReactDOM.findDOMNode(_this.refs.bcphongnguask).value.trim();
    let txmat = ReactDOM.findDOMNode(_this.refs.txmat).value.trim();
    let txda = ReactDOM.findDOMNode(_this.refs.txda).value.trim();
    let hit = ReactDOM.findDOMNode(_this.refs.hit).value.trim();
    let nuot = ReactDOM.findDOMNode(_this.refs.nuot).value.trim();
    let bcphongngua = ReactDOM.findDOMNode(_this.refs.bcphongngua).value.trim();
    let chay = ReactDOM.findDOMNode(_this.refs.chay).value.trim();
    let trannho = ReactDOM.findDOMNode(_this.refs.trannho).value.trim();
    let tranlon = ReactDOM.findDOMNode(_this.refs.tranlon).value.trim();
    let luutru = ReactDOM.findDOMNode(_this.refs.luutru).value.trim();

    /*
     * Validation rules
     */
    if (name.length < 1) {
      toastr.warning('Tittle is required.', 'NCKH');
      return false;
    }

    this.props.addBlog({
      name,cthh,dbocchay,dsang,gioihanno,ld50,lc50,pel,rel,idlh,thghs,ncghs,trieuchung,bcphongnguask,
      txmat,txda,hit,nuot,bcphongngua,chay,trannho,tranlon,luutru
    }, function (err, res) {
      if (err) {
        console.error('addBlog: ', err);
        toastr.error(err.message, 'NCKH');
      } else {
        console.log('addBlog: ', res);
        toastr.success(res.message, 'NCKH');
        browserHistory.push('/blogs/allblogs');
      }
    });
  }
}

export default AddBlogComponent;
