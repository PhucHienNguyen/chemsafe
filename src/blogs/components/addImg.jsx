import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link, browserHistory } from 'react-router';
import toastr from 'toastr';

class AddImgComponent extends Component {
  render() {
    return (
      <div className='row'>
        <div className='col-xs-12 text-center'>
          <h1 className='title'>Add Images</h1>
        </div>
        <div className='col-xs-4 col-xs-offset-4'>
          <form onSubmit={this.handleSubmit.bind(this)}>
            <div className='form-group'>
              <label htmlFor='imgGHS'>NFPA: </label>
              <input
                 name="imgGHS"
                 type="file"
                 accept="image/x-png,image/gif,image/jpeg"
                 multiple="multiple" />
            </div>
          </form>
        </div>
      </div>
    );
  }

  handleSubmit(event) {
    const _this = this;
    event.preventDefault();

    let luutru = ReactDOM.findDOMNode(_this.refs.luutru).value.trim();

    /*
     * Validation rules
     */
    if (name.length < 1) {
      toastr.warning('Tittle is required.', 'NCKH');
      return false;
    }

    this.props.addBlog({
      name,cthh,dbocchay,dsang,gioihanno,ld50,lc50,pel,rel,idlh,thghs,ncghs,trieuchung,bcphongnguask,
      txmat,txda,hit,nuot,bcphongngua,chay,trannho,tranlon,luutru
    }, function (err, res) {
      if (err) {
        console.error('addBlog: ', err);
        toastr.error(err.message, 'NCKH');
      } else {
        console.log('addBlog: ', res);
        toastr.success(res.message, 'NCKH');
        browserHistory.push('/blogs/allblogs');
      }
    });
  }
}

export default AddBlogComponent;
