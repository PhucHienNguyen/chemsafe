import React, { Component } from 'react';
import { Link } from 'react-router';
import moment from 'moment';
import toastr from 'toastr';

class BlogComponent extends Component {
  render() {
    return (
      <div className='row'>
        <div className='col-xs-10 col-xs-offset-1 text-center'>
          {this.props.oneBlog ? <h3>{this.props.oneBlog.name} - {this.props.oneBlog.cthh}</h3> : null}
          <br/>
          <div className='table-responsive'>
            <table className='table table-bordered table-hover'>
              <thead>
                <tr >
                  <th className='text-center' colSpan={3}><h4><b>Thông tin an toàn</b></h4></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td colSpan={2} className='set'><Link to='NFPA'><b>NFPA 704</b></Link></td>
                  {this.props.oneBlog ? <td><img src={this.props.ImgUrlNFPA} width='100px' alt='' /></td> : null}
                </tr>
                <tr>
                  <td colSpan={2} className='set'><b>Điểm tự bốc cháy (°C)</b></td>
                  {this.props.oneBlog ? <td>{this.props.oneBlog.dbocchay}</td> : null}
                </tr>
                <tr>
                  <td colSpan={2} className='set'><b>Điểm sáng (°C)</b></td>
                  {this.props.oneBlog ? <td>{this.props.oneBlog.dsang}</td> : null}
                </tr>
                <tr>
                  <td colSpan={2} className='set'><b>Giới hạn nổ (%)</b></td>
                  {this.props.oneBlog ? <td>{this.props.oneBlog.gioihanno}</td> : null}
                </tr>
                <tr>
                  <th rowSpan={2} className='setin1'><b>Độc tính</b></th>
                  <td className='setin2'><b><Link to='LD50'>LD<sub>50 </sub> (mg/kg)</Link></b></td>
                  <td>{this.loadLD50()}</td>
                </tr>
                <tr>
                  <td className='setin2'><b><Link to='LC50'>LC<sub>50 </sub> (ppm)</Link></b></td>
                  {this.props.oneBlog ? <td>{this.props.oneBlog.lc50}</td> : null}
                </tr>
                <tr>
                    <td className='setin1'><b>OSHA</b></td>
                    <td className=''><b>PEL (ppm)</b></td>
                  {this.props.oneBlog ? <td>{this.props.oneBlog.pel}</td> : null}
                </tr>
                <tr>
                  <th rowSpan={2} className='setin1'><b>NIOSH</b></th>
                  <td className='setin2'><b>REL (ppm)</b></td>
                  <td>{this.loadRel()}</td>
                </tr>
                <tr>
                  <td className='setin2'><b>IDLH (ppm)</b></td>
                  {this.props.oneBlog ? <td>{this.props.oneBlog.idlh}</td> : null}
                </tr>
                <tr>
                  <td colSpan={2} className='set'><b>Tín hiệu <Link to='GHS'>GHS</Link></b></td>
                  {this.props.oneBlog ? <td>{this.props.oneBlog.thghs}</td> : null}
                </tr>
                <tr>
                  <td colSpan={2} className='set'><b>Báo hiệu <Link to='GHS'>GHS</Link></b></td>
                  <td>{this.loadImgGHS()}</td>
                </tr>
                <tr>
                  <td colSpan={2} className='set'><b><Link to='NCGHS'>Nguy cơ GHS</Link></b></td>
                  {this.props.oneBlog ? <td>{this.props.oneBlog.ncghs}</td> : null}
                </tr>
                <tr>
                  <td colSpan={3} className='set'><h4><b>Phòng ngừa và xử lý sự cố</b></h4></td>
                </tr>
                <tr>
                  <td colSpan={3} className='text-justify'>
                  <h5><b>Về sức khỏe:</b></h5>
                  <h5><b>Các triệu chứng khi bị nhiễm: </b></h5>
                    {this.loadTC()}
                  <h5><b>Báo cáo phòng ngừa GHS: </b></h5>
                    {this.loadBCPNSK()}
                  <h5><b>Các biện pháp sơ cứu: </b></h5>
                  <h5><b>- Tiếp xúc với mắt: </b></h5>
                    {this.loadTxmat()}
                  <h5><b>- Tiếp xúc với da: </b></h5>
                    {this.loadTxda()}
                  <h5><b>- Hít phải:</b></h5>
                    {this.loadHit()}
                  <h5><b>- Nuốt phải:</b></h5>
                    {this.loadNuot()}
                  <h5><b>Phòng ngừa và xử lý</b></h5>
                  <h5><b>Báo cáo phòng ngừa GHS: </b></h5>
                    {this.loadBCPN()}
                  <h5><b>Sự cố cháy: (theo GHS)</b></h5>
                    {this.loadChay()}
                  <h5><b>Sự cố tràn nhỏ:</b></h5>
                    {this.loadTnho()}
                  <h5><b>Sự cố tràn lớn:</b></h5>
                    {this.loadTlon()}
                  <h5><b>Lưu trữ  (theo GHS)</b></h5>
                    {this.loadLuu()}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <br/><br/>
        <div className='col-xs-4 col-xs-offset-4 text-center'>
          <Link
            to='/blogs/addImg'
            className='btn btn-block btn-default'>
            Add Images
          </Link>
        </div>
      </div>
    );
  }
  loadImgGHS(){
    if(this.props.oneBlog && this.props.fileGet.length){
      return this.props.fileGet.map(function (img, index) {
        return (
       <img key={index} src={img} width='70px' alt='' />
        );
      });
    }
  }

  loadRel(){
    if(this.props.oneBlog){
      return this.props.oneBlog.rel.split("\n").map(function (text, index) {
        return (
       <div key={index}> {text} </div>
        );
      });
    }
  }

  loadLD50(){
    if(this.props.oneBlog){
      return this.props.oneBlog.ld50.split("\n").map(function (text, index) {
        return (
       <div key={index}> {text} </div>
        );
      });
    }
  }

  loadTC(){
    if(this.props.oneBlog){
      return this.props.oneBlog.trieuchung.split("\n").map(function (text, index) {
        return (
       <div key={index}> {text} </div>
        );
      });
    }
  }
  loadBCPNSK(){
    if(this.props.oneBlog){
      return this.props.oneBlog.bcphongnguask.split("\n").map(function (text, index) {
        return (
       <div key={index}> {text} </div>
        );
      });
    }
  }
  loadTxmat(){
    if(this.props.oneBlog){
      return this.props.oneBlog.txmat.split("\n").map(function (text, index) {
        return (
       <div key={index}> {text} </div>
        );
      });
    }
  }
  loadTxda(){
    if(this.props.oneBlog){
      return this.props.oneBlog.txda.split("\n").map(function (text, index) {
        return (
       <div key={index}> {text} </div>
        );
      });
    }
  }
  loadHit(){
    if(this.props.oneBlog){
      return this.props.oneBlog.hit.split("\n").map(function (text, index) {
        return (
       <div key={index}> {text} </div>
        );
      });
    }
  }
  loadNuot(){
    if(this.props.oneBlog){
      return this.props.oneBlog.nuot.split("\n").map(function (text, index) {
        return (
       <div key={index}> {text} </div>
        );
      });
    }
  }
  loadBCPN(){
    if(this.props.oneBlog){
      return this.props.oneBlog.bcphongngua.split("\n").map(function (text, index) {
        return (
       <div key={index}> {text} </div>
        );
      });
    }
  }
  loadChay(){
    if(this.props.oneBlog){
      return this.props.oneBlog.chay.split("\n").map(function (text, index) {
        return (
       <div key={index}> {text} </div>
        );
      });
    }
  }
  loadTnho(){
    if(this.props.oneBlog){
      return this.props.oneBlog.trannho.split("\n").map(function (text, index) {
        return (
       <div key={index}> {text} </div>
        );
      });
    }
  }
  loadTlon(){
    if(this.props.oneBlog){
      return this.props.oneBlog.tranlon.split("\n").map(function (text, index) {
        return (
       <div key={index}> {text} </div>
        );
      });
    }
  }
  loadLuu(){
    if(this.props.oneBlog){
      return this.props.oneBlog.luutru.split("\n").map(function (text, index) {
        return (
       <div key={index}> {text} </div>
        );
      });
    }
  }

}
export default BlogComponent;
