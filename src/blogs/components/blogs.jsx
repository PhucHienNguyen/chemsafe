import React, { Component } from 'react';
import { Link } from 'react-router';
import SearchBar from './searchBar.jsx'
import moment from 'moment';
import toastr from 'toastr';

class BlogComponent extends Component {
  render() {
    return (
      <div className='row'>
        <div className='col-xs-8 col-xs-offset-2 text-center'>
          <div className='table-responsive'>
            <table className='table table-bordered table-hover'>
              <thead>
                <tr>
                  <th className='set'>Tên chất</th>
                  <th className='set'>CTHH</th>
                </tr>
              </thead>
              <tbody>
                {this.renderList()}
              </tbody>
            </table>
          </div>
        </div>
        <br/><br/>
      {/*   <div className='col-xs-4 col-xs-offset-4 text-center'>
          <Link
            to='/blogs/addblog'
            className='btn btn-block btn-default'>
            Add new
          </Link>
        </div>
      */}
      </div>
    );
  }
  handleSubmit(e) {
    event.preventDefault();
    let id = e;
    console.log(id);
    this.props.getInfoBlog({id},function (err, res) {
      if (err) {
        console.error('Get: ', err);
        toastr.error(err.message, 'NCKH');
    }});
  }
  renderList() {
      let _this = this;

      if (this.props.allBlog.length) {
        return this.props.allBlog.map(function (blog, index) {
          return (
            <tr key = {blog._id}>
              <td>
              <Link to={`/blogs/blog/${blog._id}`}
              >
              {blog.name}
              </Link>
              </td>
              <td>{blog.cthh}</td>
            </tr>
          );
        });
      } else {
        return null;
      }
  }

}

export default BlogComponent;
