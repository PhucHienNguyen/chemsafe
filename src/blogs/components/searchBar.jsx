import React, { Component } from 'react';
import moment from 'moment';
import toastr from 'toastr';

class SearchBar extends Component {
  render() {
    return (
      <div className='row'>
        <div className='col-xs-4 col-xs-offset-4'>
            <div className='form-group'>
              <label htmlFor='search'>Search</label>
              <input
                ref='search'
                type='text'
                className='form-control'
                placeholder='Search'
                onChange={this.handleChange.bind(this)} />
            </div>
        </div>
        </div>
      );
    }
    handleChange(event){
      event.preventDefault();
      console.log(event.target.value);
      let name = event.target.value.trim();
      this.props.getBlogbySearch({name},function (err, res) {
        if (err) {
          console.error('Search: ', err);
          toastr.error(err.message, 'NCKH');
        }});
    }
  }
  export default SearchBar;
