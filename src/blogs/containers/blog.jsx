import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import OneBlogComponent from '../components/blog.jsx';

import { getInfoBlog, getFile } from '../actions/getBlog.js';

class BlogContainer extends Component {
  componentWillMount(){
    let temp = '/images/' + this.props.params.blogId + '/BHGHS';
    this.props.getFile(temp);
    let ImgUrlNFPA = '/images/' + this.props.params.blogId + '/NFPA/NFPA.png';
    this.setState({ImgUrlNFPA: ImgUrlNFPA});
    this.props.getInfoBlog(this.props.params.blogId);
  }
  render() {
    return (
      <div>
        <OneBlogComponent
          ImgUrlNFPA = {this.state.ImgUrlNFPA}
          fileGet = {this.props.fileGet}
          oneBlog={this.props.oneBlog} />
      </div>
    );
  }
}

// Get apps store and pass it as props to BlogContainer
//  > whenever store changes, the BlogContainer will automatically re-render
// "store.allBlog" is set in reducers.js
function mapStateToProps(store) {
  return {
    oneBlog: store.oneBlog,
    fileGet: store.fileGet,
  };
}

// Get actions and pass them as props to to BlogContainer
//  > now BlogContainer has this.props.getAllBlog
function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    getFile: getFile,
    getInfoBlog: getInfoBlog,
  }, dispatch);
}

// We don't want to return the plain BlogContainer (component) anymore,
// we want to return the smart Container
//  > BlogContainer is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(BlogContainer);
