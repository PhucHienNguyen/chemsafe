export const addBlogReducer = (state = [], action) => {
  switch (action.type) {
    case 'ADD_BLOG_REQUEST':
      return state;
    case 'ADD_BLOG_SUCCESS':
      return action.payload.data;
    case 'ADD_BLOG_FAILURE':
      return state;
    default:
      return state;
  }
};
