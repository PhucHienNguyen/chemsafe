/*
 * This reducer will always return an array of users no matter what
 * You need to return something, so if there are no users then just return an empty array
 */

export const fileReducer = (state = null, action) => {
  switch (action.type) {
    case 'GET_FILE_REQUEST':
      return state;
    case 'GET_FILE_SUCCESS':
      return action.payload.data;
    case 'GET_FILE_FAILURE':
      return state;
    default:
      return state;
  }
};
