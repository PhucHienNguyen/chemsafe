import React from 'react';
import { Route, IndexRedirect } from 'react-router';

import AllBlogContainer from '../containers/blogs.jsx';
import AddBlogContainer from '../containers/addBlogs.jsx'
import BlogContainer from '../containers/blog.jsx'

export default function () {
  return (
    <Route path='blogs'>
      <IndexRedirect to='allblogs' />
      <Route path='allblogs' component={AllBlogContainer} />
      <Route path='addblog' component={AddBlogContainer} />
      <Route path='blog/:blogId' component={BlogContainer} />
    </Route>
  );
};
