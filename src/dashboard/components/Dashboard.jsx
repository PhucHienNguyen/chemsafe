import React, { Component } from 'react';
import toastr from 'toastr';
import { Link } from 'react-router';

class DashboardComponent extends Component {
  componentDidMount() {
    toastr.info('Welcome to Chemsafe Team.');
  }

  render() {
    return (
      <div className='row'>
        <div className='col-xs-12 text-center'>
          <img src='/images/dashboard.jpg' width='150px' alt='' />
          <h1>Welcome to Chemsafe Team</h1>
          <p><small>2017</small></p>
          <br /><br />
        </div>
        <div className='col-xs-12 text-center'>
        How to search chemistry names?
        <br />
        <Link to='/blogs'>Click me!!!</Link>
        </div>
      </div>
    );
  }
}

export default DashboardComponent;
