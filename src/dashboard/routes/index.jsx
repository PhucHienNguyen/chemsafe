import React from 'react';
import { Route, IndexRoute } from 'react-router';
import toastr from 'toastr';

import DashboardContainer from '../containers/Dashboard.jsx';


export default function () {
  return (
    <Route path='dashboard'>
      <IndexRoute component={DashboardContainer} />
    </Route>
  );
}
