import { combineReducers } from 'redux';


import { allBlogReducer } from './blogs/reducers/allBlog.js';
import { oneBlogReducer } from './blogs/reducers/oneBlog.js';
import { fileReducer } from './blogs/reducers/fileGet.js';

const reducers = combineReducers({
  allBlog: allBlogReducer,
  oneBlog: oneBlogReducer,
  fileGet: fileReducer,
});

export default reducers;
