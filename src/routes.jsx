import React, { Component } from 'react';
import { Router, Route, IndexRoute, IndexRedirect, browserHistory } from 'react-router';

import CoreLayout from './core/layouts/index.jsx';
import NotFoundComponent from './core/components/notFound.jsx';

import dashboardRoute from './dashboard/routes/index.jsx';
import blogRoute from './blogs/routes/index.jsx';
import NFPARoute from './NFPA/routes/index.jsx';
import GHSRoute from './GHS/routes/index.jsx';
import NCGHSRoute from './NCGHS/routes/index.jsx';
import LC50Route from './LC50/routes/index.jsx';
import LD50Route from './LD50/routes/index.jsx';

class Routes extends Component {
  render() {
    return (
      <Router history={browserHistory}>
        <Route path='/' component={CoreLayout}>
          <IndexRedirect to='dashboard'/>
          {dashboardRoute()}
          {blogRoute()}
          {NFPARoute()}
          {GHSRoute()}
          {LC50Route()}
          {LD50Route()}
          {NCGHSRoute()}
          <Route path='*' component={NotFoundComponent} />
        </Route>
      </Router>
    );
  }
}

export default Routes;
